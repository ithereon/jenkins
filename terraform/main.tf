
provider "aws" {
  region = "eu-central-1"
}

##################################################################
# Data sources to get VPC, subnet, security group and AMI details
##################################################################
resource "aws_vpc" "jenkins_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = "jenkins"
  }
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "jenkins-sg"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "all-icmp","ssh-myIp"]
  egress_rules        = ["all-all"]
}

module "ec2" {
 source = "terraform-aws-modules/ec2-instance/aws"

  count = var.instances_number

  name                        = "jenkins-instance"
  ami                         = data.aws_ami.amazon_linux.id
  instance_type               = "t2.micro"
  subnet_id                   = tolist(data.aws_subnet_ids.all.ids)[0]
  vpc_security_group_ids      = [module.security_group.security_group_id]
  associate_public_ip_address = true
}

resource "aws_key_pair" "my-machine" {
  key_name   = "linux-key"
  public_key = "sh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMhX61f4IcUuwa8WxREjpW8eZVA/KbpqeSGHzlxTTT6+ Gitlab Key"
}